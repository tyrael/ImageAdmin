Image Admin
=======

Admin image by upload them to cloud storage and show them in local web admin page


## Usage

**prepare**

```bash
pip install -r requirements.txt
```

make sure you have run a mongodb anywhere and config in `config/default.py`

**run server**

```bash
python manager -c production runserver
```

**upload api**

```python
import requests

url = "http://localhost:5000"
files = {'smfile': open(local_path, 'rb')}
resp = json.loads(requests.post(url, files=files).text)
if resp.get('code') == 'success':
	print "success"
else:
    print "fail"
```

## Dependence

    MongoDB >= 3.0.0

## Where to used

#### Windows

you can use it by [SharedX](https://github.com/ShareX/ShareX), and i have push an customer uploader config: [sm.ms.json](https://github.com/ShareX/CustomUploaders/blob/master/sm.ms.json)

#### Run alone

you can only run it alone for manager your uploaded images.

**TODO**

visit `http://localhost:5000/images` can view all images you ever uploaded.