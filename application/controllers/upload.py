#!/usr/bin/env python
# encoding: utf-8
import os

import requests
from flask import (jsonify, current_app, Blueprint, request,
                   flash, redirect, send_from_directory)
from werkzeug.utils import secure_filename

from application.extensions import mg


upload_bp = Blueprint('upload', __name__, url_prefix='/upload')


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def upload_to_sm(local_path):
    url = 'https://sm.ms/api/upload'
    files = {'smfile': open(local_path, 'rb')}
    r = requests.post(url, files=files)
    data1 = eval(r.text.encode('utf-8'))
    return data1


@upload_bp.route('/', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'smfile' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['smfile']
    # if user does not select file, browser also
    # submit a empty part without filename
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        local_path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
        file.save(local_path)
        resp = upload_to_sm(local_path)
        print "uplaod with resp: {}".format(resp)
        mg.db.smms.insert(resp)
        del resp['_id']
        return jsonify(resp)
    return ''


@upload_bp.route('/<filename>')
def uploaded_file(filename):
    return send_from_directory(current_app.config['UPLOAD_FOLDER'], filename)
